from setuptools import setup

setup(name='gym_tf', 
    version='0.0.1', 
    install_requires=['gym', 'numpy', 'matplotlib', 'control', 'scipy']
)