# gym-tf

A tf environment is a module being developed for tuning a controller for a process by giving its *_transfer function_* using RL.

## References

1. [Creating a custom OpenAI gym environment](https://github.com/openai/gym/blob/master/docs/creating-environments.md).
2. [How to create a new gym environment in OpenAI? - Stackoverflow](https://stackoverflow.com/questions/45068568/how-to-create-a-new-gym-environment-in-openai).
3. [Creating your own RL environment](https://towardsdatascience.com/create-your-own-reinforcement-learning-environment-beb12f4151ef).