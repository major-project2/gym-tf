from gym.envs.registration import register

register(
    id='tf-v0',
    entry_point='gym_tf.envs:TFEnv',
)

''' Parameters that can be added in the future
timestep_limit=1000,
reward_threshold=1.0,
nondeterministic = True,
'''