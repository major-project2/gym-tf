import gym
from gym import error, spaces, utils
from gym.utils import seeding
import os
import numpy as np
from control import matlab as matlb
import scipy.signal as sgn
from matplotlib import pyplot as plt 

class TFEnv(gym.Env):
    metadata = {'render.modes': ['human']}

    #############################################################################################################################################################
    # Function Name -   _pid(self, Kd, Ki, Kp) 
    # Parameters    -   self, Kd, Ki, Kp 
    # Purpose       -   To create a transfer function of a PID Controller with given PID Gains
    # Output        -   Transfer Function of PID Controller with given PID gains
    #############################################################################################################################################################

    def _pid(self, Kd, Ki, Kp):
        # Create a PID Transfer Function
        G_PID = matlb.tf([Kd, Kp, Ki], [1, 0])
        # Return Object
        return G_PID

    #############################################################################################################################################################
    # Function Name -   Simulate_Step_Response(self, Time, G1, SP, Sig, Ld, Ld_Mag, freq, Kd, Ki, Kp)  
    # Parameters    -   self, Time (Simulation Time), G1 (Transfer Function), SP (Magnitude of setpoint), Ld (Type of Load (Not of use as of now))
    #                   Ld_Mag (Magnitude of load (Not of use as of now)), Kd, Ki, Kp 
    # Purpose       -   To simulate th step and forced response of the given transfer function
    # Output        -   System Response (y), Time (t), State Variables
    #############################################################################################################################################################

    def Simulate_Step_Response(self, Time, G1, SP, Sig, Ld, Ld_Mag, freq, phase, Kd, Ki, Kp):

        # Sample Time
        t_samp = np.linspace(0, Time, 10*Time)

        # Setpoint Signal
        if(Sig == 1):
            # Constant Function
            In_Sig = SP * np.ones(len(t_samp))
        elif(Sig == 2):
            # Square
            In_Sig = (SP/2) + (SP/2) * sgn.square(self.SP_freq*0.25*t_samp + phase)
        elif(Sig== 3):
            # Saw tooth
            In_Sig = (SP/2) + (SP/2) * sgn.sawtooth(self.SP_freq*0.25*t_samp + phase)
        elif(Sig == 4):
            # Triangle Function
            In_Sig = (SP/2) + (SP/2) * sgn.sawtooth(self.SP_freq*0.25*t_samp + phase, 0.5)
        elif(Sig == 5):
            # Custom Signal 1 Function
            new_signal = self.Custom_signal_1
            In_Sig = SP*new_signal
        elif(Sig == 6):
            # Custom Signal 2 Function
            new_signal = self.Custom_signal_2
            In_Sig = SP*new_signal
        elif(Sig == 7):
            # Custom Signal 3 Function
            new_signal = self.Custom_signal_3
            In_Sig = SP*new_signal

        # Set Point signal
        sp = In_Sig
        self.setpoint_sgn = In_Sig

        # Creating a PID Transfer Function
        g_pid = self._pid(Kd, Ki, Kp)

        # Initializing the g_load
        g_load = None

        # Creating a Load Block
        if(Ld == 1):
            # Step Function
            g_load = matlb.tf([Ld_Mag], [1, 0])
        elif(Ld == 2):
            # Ramp Function
            g_load = matlb.tf([1], [1, 0, 0])
            m = Ld_Mag*np.ones(len(t_samp))
        elif(Ld == 3):
            # Sine Function
            g_load = matlb.tf([Ld_Mag*freq], [1, 0, freq**2])
            Ld_pnts = Ld_Mag * np.sin(freq*t_samp)
        elif(Ld == 4):
            # Cosine Function
            g_load = matlb.tf([Ld_Mag, 0], [1, 0, freq**2])

        # Adding the output of the blocks
        g_pid = matlb.parallel(g_pid, g_load)

        # Transfer Function of a Feedback System
        g_final = matlb.feedback(g_pid*G1, 1)

        # Step response of the system
        y, t, x = matlb.lsim(g_final,sp,t_samp)

        return y, t, x

    #############################################################################################################################################################
    # Setter to set the parameters mentioned below
    # Paramters - SP Signal Type, Process Transfer Function, Simulation time, Setpoint magnitude, Frequency of SP signal, MSE Thresh, Phase
    #############################################################################################################################################################

    def set_SP_sgn_type(self, sgn_id):
        self.SP_sgn_type = int(sgn_id)

    def set_Process(self, num, den):
        self.Process_TF = matlb.tf(num, den)

    def set_sim_time(self, sim_t):
        self.self.sim_t = sim_t

    def set_setpoint(self, sp):
        self.setpoint = sp

    def set_SP_Freq(self, sp_freq):
        self.SP_freq = sp_freq

    def set_SP_Phase(self, sp_phase):
        self.SP_phase = sp_phase

    def set_MSE_thresh(self, thresh):
        self.MSE_Thresh = thresh

    #############################################################################################################################################################
    # getters to set the parameters mentioned below
    # Paramters - SP Signal Type, Process Transfer Function, Simulation time, Setpoint magnitude, Frequency of SP signal, MSE Thresh, Phase
    #############################################################################################################################################################

    def get_SP_sgn_type(self):
        if(self.SP_sgn_type == 1):
            return 'Constant'
        elif(self.SP_sgn_type == 2):
            return 'Square'
        elif(self.SP_sgn_type == 3):
            return 'Sawtooth'
        elif(self.SP_sgn_type == 4):
            return 'Triangle'
        elif(self.SP_sgn_type == 5):
            return 'Custom_1'
        elif(self.SP_sgn_type == 6):
            return 'Custom_2'
        elif(self.SP_sgn_type == 7):
            return 'Custom_3'

    def get_SP_sgn(self, SP_sgn_id):
        if(int(SP_sgn_id) == 1):
            return np.array(self.setpoint * np.ones(10*self.sim_t))
        elif(int(SP_sgn_id)== 2):
            return np.array((self.setpoint/2) + (self.setpoint/2) * sgn.square(self.SP_freq*0.25*np.linspace(0, self.sim_t, 10*self.sim_t) + self.SP_phase))
        elif(int(SP_sgn_id) == 3):
            return np.array((self.setpoint/2) + (self.setpoint/2) * sgn.sawtooth(self.SP_freq*0.25*np.linspace(0, self.sim_t, 10*self.sim_t) + self.SP_phase))
        elif(int(SP_sgn_id) == 4):
            return np.array((self.setpoint/2) + (self.setpoint/2) * sgn.sawtooth(self.SP_freq*0.25*np.linspace(0, self.sim_t, 10*self.sim_t) + self.SP_phase, 0.5))
        elif(int(SP_sgn_id) == 5):
            return self.setpoint*np.array(self.Custom_signal_1)
        elif(int(SP_sgn_id) == 6):
            return self.setpoint*np.array(self.Custom_signal_2)
        elif(int(SP_sgn_id) == 7):
            return self.setpoint*np.array(self.Custom_signal_3)

    def get_Process(self):
        return self.Process_TF

    def get_PID_Gains(self):
        return np.array([self.Kp, self.Ki, self.Kd])

    def get_MSE(self):
        return self.MSE

    def get_MSE_thresh(self):
        return self.MSE_Thresh

    def get_SP_Freq(self):
        return self.SP_freq 

    def get_SP_Phase(self):
        return self.SP_phase

    def get_sim_time(self):
        return self.sim_t

    def get_setpoint(self):
        return self.setpoint

    def __init__(self):
        # Minimum values of the PID Gains
        self.min_Kp = 0
        self.min_Ki = 0
        self.min_Kd = 0
        self.min_Err = 0

        # Minimum values of the PID Gains
        self.high_Kp = 50
        self.high_Ki = 50
        self.high_Kd = 50
        self.high_Err = 50

        # Simulation Parameters
        self.sim_t = 200
        self.setpoint = 1
        self.SP_freq = 1
        self.SP_phase = 0
        self.setpoint_sgn = self.setpoint*np.linspace(0, self.sim_t, 10*self.sim_t)
        self.SP_sgn_type = 1
        self.Ld_type = 1
        self.Ld_Mag = 0
        self.freq = 1

        # Termination Parameters
        self.control_band = 0.05 
        self.Up_Limit = (1 + self.control_band )*self.setpoint_sgn
        self.Down_Limit = (1 - self.control_band )*self.setpoint_sgn
        self.MSE_Thresh = 0.075
        self.stdy_st_ridings = 100
        self.stdy_st_Err_Thres = 0.05

        # Custom Setpoint signal
        self.Pulse_a = np.ones(int((10*self.sim_t)/(2*5)))
        self.Pulse_b = np.zeros(int((10*self.sim_t)/(2*5)))
        self.Custom_signal_1 = np.concatenate((0.5*self.Pulse_a, self.Pulse_a, 1.5*self.Pulse_a, 0.5*self.Pulse_a, self.Pulse_b, 0.5*self.Pulse_a, self.Pulse_a, 1.5*self.Pulse_a, 0.5*self.Pulse_a, self.Pulse_b), axis=0)

        self.Pulse_c = np.linspace(0, 1, 200)
        self.Custom_signal_2 = np.concatenate((self.Pulse_c, self.Pulse_a, np.add(1, self.Pulse_c), 2*self.Pulse_a, np.linspace(2, 0, 200), self.Pulse_b, self.Pulse_c, self.Pulse_a, np.add(1, self.Pulse_c), 2*self.Pulse_a), axis=0)

        self.Custom_signal_3 = np.concatenate((0.5*self.Pulse_a, self.Pulse_a, 1.5*self.Pulse_a, 0.5*self.Pulse_a, np.linspace(0, 0.5, 200), 0.5*self.Pulse_a, np.linspace(0.5, 1.5, 200), 1.5*self.Pulse_a, np.linspace(1.5, 1, 200), self.Pulse_a), axis=0)

        # Increment Step
        self.Stp_Inc = 1

        # Increment in PID Gains
        self.P_Inc = [-1,-1,-1, 0,-1, 0, 0, 0,-1,-1, 1,-1, 1, 1, 1,-1, 0, 1, 0, 1,-1, 0, 0, 1, 1, 1, 0]
        self.I_Inc = [-1,-1, 0,-1, 0, 0,-1, 0,-1, 1,-1, 1, 1,-1, 1, 0,-1,-1, 1, 0, 1, 0, 1, 0, 1, 0, 1]
        self.D_Inc = [-1, 0,-1,-1, 0,-1, 0, 0, 1,-1,-1, 1,-1, 1, 1, 1, 1, 0,-1,-1, 0, 1, 0, 0, 0, 1, 1]

        # Update Array
        self.P_Inc = self.Stp_Inc * np.array(self.P_Inc)
        self.I_Inc = self.Stp_Inc * np.array(self.I_Inc)
        self.D_Inc = self.Stp_Inc * np.array(self.D_Inc)

        # Unique Identifier for Saving File
        self.file_id = 1

        # Creating a transfer function of Process
        self.Process_TF = matlb.tf([6], [1, 6, 11, 6])

        # Setting Upper and Lower limits for the State vector
        self.low = np.array([self.min_Kp, self.min_Ki, self.min_Kd, self.min_Err], dtype=np.float32)
        self.high = np.array([self.high_Kp, self.high_Ki, self.high_Kd, self.high_Err], dtype=np.float32)

        # Creating an action space and observation space
        self.action_space = spaces.Discrete(27)
        self.observation_space = spaces.Box(self.low, self.high, dtype=np.float32)

        # Initializing the plot as None
        self.fig = None

        # Enabling the interactive mode
        plt.ion()

        #Declaring the functions
        self.seed()
        self.reset()

    def seed(self, seed=None):
        self.np_random, seed = seeding.np_random(seed)
        return [seed]

    def step(self, action):

        err_msg = "%r (%s) invalid" % (action, type(action))
        assert self.action_space.contains(action), err_msg

        # Assigning the current state of the system
        self.Kp, self.Ki, self.Kd, self.MSE = self.state

        # Initializing the reward
        reward = 0

        # Updating the states according to the actions
        self.Kp += self.P_Inc[int(action)]
        self.Ki += self.I_Inc[int(action)]
        self.Kd += self.D_Inc[int(action)]

        # Updating the reward after updating the PID Gains
        reward += -10

        # System response
        self.sys_res, self.samp_t, self.st_vec = self.Simulate_Step_Response(self.sim_t, self.Process_TF, self.setpoint, self.SP_sgn_type, self.Ld_type, self.Ld_Mag, self.freq, self.SP_phase, self.Kd, self.Ki, self.Kp)

        # Calculate error
        self.Sqr_Err = np.power(np.subtract(self.sys_res, self.setpoint_sgn), 2).astype('float128')
        self.MSE = self.Sqr_Err.mean()

        # Clamping the PID Gains
        # Clamping to the lower limit
        if(self.Kp < self.min_Kp): self.Kp = self.min_Kp
        if(self.Ki < self.min_Ki): self.Ki = self.min_Ki
        if(self.Kd < self.min_Kd): self.Kd = self.min_Kd
        if(self.MSE < self.min_Kd): self.MSE = self.min_Err

        # Clamping to the upper limit
        if(self.Kp > self.high_Kp): self.Kp = self.high_Kp
        if(self.Ki > self.high_Ki): self.Ki = self.high_Ki
        if(self.Kd > self.high_Kd): self.Kd = self.high_Kd
        if(self.MSE > self.high_Kd): self.MSE = self.high_Err

        # Create a state vector from the current states
        self.state = np.array([self.Kp, self.Ki, self.Kd, self.MSE])

        # Updating the done according to the termination condition and type of setpoint signal given
        # Accordingly updating the reward
        if(self.SP_sgn_type == 1):
            #If the setpoint signal is constant use this done rule
            done = bool(np.max(self.Sqr_Err) < ((self.control_band*self.setpoint)**2) and 
                        self.MSE < (self.MSE_Thresh) and
                        np.sum(self.Sqr_Err[self.stdy_st_ridings:]) < (self.stdy_st_Err_Thres))

            # Updating the Reward based on System response being in the control band
            if(np.max(self.Sqr_Err) < (self.control_band*self.setpoint)**2):
                reward += 50
            else:
                reward += -10

            # Updating the Reward based on System response being as near as setpoint
            if(self.MSE < self.MSE_Thresh):
                reward += 30
            else:
                reward += -10

            # Updating the Reward based on System response being as stable as possible
            if(np.sum(self.Sqr_Err[self.stdy_st_ridings:]) < self.stdy_st_Err_Thres):
                reward += 10
            else:
                reward += -10

        else:
            #If the setpoint signal which is a pulse use this done rule
            done = bool(np.max(self.Sqr_Err) < ((self.control_band*self.setpoint)**2) and 
                        self.MSE < (self.MSE_Thresh))

            # Updating the Reward based on System response being in the control band
            if(np.max(self.Sqr_Err) < (self.control_band*self.setpoint)**2):
                reward += 50
            else:
                reward += -10

            # Updating the Reward based on System response being as near as setpoint
            if(self.MSE < self.MSE_Thresh):
                reward += 40
            else:
                reward += -20

        # Assigning the reward
        if done:
            reward += 500

        # Return state, reward, done, info
        return self.state, reward, done, {}

    def reset(self):
        # Randomly Initializing Kp, Ki, Kd
        self.Kp, self.Ki ,self.Kd = self.np_random.randint(16, size=(3))

        # System response
        self.sys_res, self.samp_t, self.st_vec = self.Simulate_Step_Response(self.sim_t, self.Process_TF, self.setpoint, self.SP_sgn_type, self.Ld_type, self.Ld_Mag, self.freq, self.SP_phase, self.Kd, self.Ki, self.Kp)
        
        # Calculate error
        self.Sqr_Err = np.power(np.subtract(self.sys_res, self.setpoint_sgn), 2).astype('float128')
        self.MSE = self.Sqr_Err.mean()
        
        # Create a state vector
        self.state = np.array([self.Kp, self.Ki, self.Kd, self.MSE])

        return np.array(self.state)

    #############################################################################################################################################################
    # Function Name -   plot_response(self, plt_type, param, tolerance, save)
    # Parameters    -   self, 
    #                   plt_type        -   1 -> Plots both error and system response, 2 -> Plots either error or system response 
    #                   param           -   Needed when plt_type = 2. 1 -> Plot System Response. 2 -> Plot Error. 
    #                   tolerance       -   1 -> Plot Tolerance Band. 0 -> Don't Plot Tolerance Band
    #                   save            -   1 -> Save Initial and final response of the system. 0 -> Don't Save.
    # Purpose       -   To simulate th step and forced response of the given transfer function
    # Output        -   System Response (y), Time (t), State Variables
    #############################################################################################################################################################

    def plot_response(self, plt_type, param, tolerance, save):

        if(self.fig == None):

            # Create a figure
            self.fig = plt.figure()

        # Clearing the figure
        self.fig.clf()

        # First Display Mode
        if(int(plt_type)==1):

            # Create a grid space
            self.grdspc = self.fig.add_gridspec(2, 2)

            # Create axes to plt graphs
            self.ax1 = self.fig.add_subplot(self.grdspc[0, :])
            self.ax2 = self.fig.add_subplot(self.grdspc[1, :])

            # Plot System Response
            self.ax1.plot(self.samp_t, self.sys_res, 'r-', label='Step Response')
            self.ax1.plot(self.samp_t, self.setpoint_sgn, 'k--', label='Setpoint')

            # Plot Error
            # Absolute of the error is plotted and logged in the data file.
            self.ax2.plot(self.samp_t, np.abs(np.subtract(self.sys_res, self.setpoint_sgn)), 'g-')
            self.ax2.plot(self.samp_t, np.zeros(len(self.samp_t)), 'b:')     

            # Adding the Title
            self.ax1.set_title('System Response')
            self.ax2.set_title('Error')

            # Adding the X axis label
            self.ax1.set_xlabel('Time (min)')
            self.ax2.set_xlabel('Time (min)')

            # Adding the X axis label
            self.ax1.set_ylabel('Amplittude')
            self.ax2.set_ylabel('Amplittude')

            # Adding space around the subplots
            self.fig.tight_layout(pad=0.2)

            # Legend of the Plots
            self.ax1.legend()

            # Enabling Grid on the plots
            self.ax1.grid('True', linestyle=":")
            self.ax2.grid('True', linestyle=":")

        # Second Display Mode
        elif(int(plt_type)==2):

            # Create a grid space
            self.grdspc = self.fig.add_gridspec(1, 2)

            # Create axes to plt graphs
            self.ax1 = self.fig.add_subplot(self.grdspc[0, :])

            # System Response Display Mode
            if(int(param)==1):

                # Plot System Response
                if(int(tolerance) == 1):
                    # Plotting Tolerance Band
                    self.ax1.plot(self.samp_t, (1 + self.control_band )*self.setpoint_sgn, 'b:', label='Tolerance Band')
                    self.ax1.plot(self.samp_t, (1 - self.control_band )*self.setpoint_sgn, 'b:')
                self.ax1.plot(self.samp_t, self.setpoint_sgn, 'k--', label='Setpoint')
                self.ax1.plot(self.samp_t, self.sys_res, 'r-', label='Step Response')


            # Error Display Mode
            elif(int(param)==2):

                # Plot Error
                if(int(tolerance) == 1):
                    # Plotting Tolerance Band
                    self.ax1.plot(self.samp_t, self.control_band*self.setpoint*np.ones(len(self.samp_t)), 'b:', label='Tolerance')
                self.ax1.plot(self.samp_t, np.zeros(len(self.samp_t)), 'k--', label='Reference')
                self.ax1.plot(self.samp_t, np.abs(np.subtract(self.sys_res, self.setpoint_sgn)), 'g-', label='Actual Error')

            # Adding the Title
            self.ax1.set_title('Error')

            # Adding the X axis label
            self.ax1.set_xlabel('Time (min)')

            # Adding the X axis label
            self.ax1.set_ylabel('Amplittude')

            # Adding space around the subplots
            self.fig.tight_layout(pad=0.2)

            # Legend of the Plots
            self.ax1.legend()

            # Enabling Grid on the plots
            self.ax1.grid('True', linestyle=":")

        # Pause for a small amount of time
        plt.pause(0.01)

        # Save Parameter
        if(int(save)==1):

            # Name of the Response Plot
            filename = 'Response_Plot_' + str(self.file_id)

            # Creating absolute path for the file
            abs_filename = os.path.join(os.getcwd(),filename)

            # If the mentioned path does not exist save the file with same name
            if not os.path.exists(abs_filename + '.png'):
                pass
            else:
                abs_filename = abs_filename + '_Final'

                # Unique Identifier
                self.file_id += 1

            # Save the file at given location
            self.fig.savefig(abs_filename + '.png', dpi=300, bbox_inches='tight')

    def render(self, plt_type, param, tolerance, save):

        # Plot the response
        self.plot_response(int(plt_type), int(param), int(tolerance), int(save))

    def close(self):
        if self.fig:
            plt.close(self.fig)
            plt.ioff()
            self.fig = None